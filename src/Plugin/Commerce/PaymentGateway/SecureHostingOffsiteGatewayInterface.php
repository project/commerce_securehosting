<?php

namespace Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface SecureHostingOffsiteGatewayInterface.
 */
interface SecureHostingOffsiteGatewayInterface extends SecureHostingGatewayInterface {

  /**
   * Whether the gateway auto-submits the form.
   *
   * @return bool
   *   TRUE if it auto-submits.
   */
  public function autoSubmits();

  /**
   * How long to wait before submitting.
   *
   * @return int
   *   The timeout in milliseconds.
   */
  public function getAutoSubmitTimeout();

  /**
   * The referrer url.
   *
   * @return string
   *   The referrer url.
   */
  public function getRefererUrl();

  /**
   * Builds the transaction data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The commerce payment object.
   * @param array $form
   *   The commerce redirect form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The checkout redirect form state.
   *
   * @return bool|array
   *   Transaction data.
   */
  public function buildFormData(PaymentInterface $payment, array $form, FormStateInterface $form_state);

}
