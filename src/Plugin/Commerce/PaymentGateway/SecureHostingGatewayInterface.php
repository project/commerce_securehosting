<?php

namespace Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway;

/**
 * Interface SecureHostingGatewayInterface.
 */
interface SecureHostingGatewayInterface {

  /**
   * Gets the gateway URL.
   *
   * @return string
   *   The redirect URL.
   */
  public function getGatewayUrl();

  /**
   * Gets whether the gateway is operating in test mode.
   *
   * @return bool
   *   The state.
   */
  public function isTest();

  /**
   * Get the render array for test transactions.
   *
   * @see https://www.securehosting.com/download/SHP-Technical-Integration-Guide.pdf
   * Check Appendix C
   * @see https://www.securehosting.com/download/Monek_test_Script3.0.3.pdf
   */
  public function helpRenderArray();

}
