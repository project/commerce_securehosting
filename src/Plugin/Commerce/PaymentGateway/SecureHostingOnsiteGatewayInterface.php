<?php

namespace Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway;

/**
 * Interface SecureHostingOnsiteGatewayInterface.
 */
interface SecureHostingOnsiteGatewayInterface extends SecureHostingGatewayInterface {

}
