<?php

namespace Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\Core\Url;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Trait for secure hosting gateway.
 *
 * Contains common functionality used by both the on-site and off-site gateways.
 */
trait SecureHostingGatewayTrait {

  /**
   * The currency repository.
   *
   * @var \CommerceGuys\Intl\Currency\CurrencyRepositoryInterface
   */
  protected $currencyRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Get the remote transaction number of a payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @return string
   *   The Secure Hosting transaction number.
   */
  protected function getTransactionNumber(PaymentInterface $payment) {
    return explode('|', $payment->getRemoteId())[0];
  }

  /**
   * Creates a composite Remote ID from transaction fields.
   *
   * @param string $reference
   *   The transaction reference.
   * @param string $cv2_result
   *   The result of the transaction.
   * @param string $card_type
   *   The card type.
   * @param string $auth_code
   *   The authentication code.
   *
   * @return string
   *   ID in format: "<reference>|<cv2avsresult|cardtype|auth_code>".
   */
  protected function prepareRemoteId(string $reference, string $cv2_result, string $card_type, string $auth_code = NULL) {
    return $reference . '|' . $cv2_result . '|' . $card_type . '|' . $auth_code;
  }

  /**
   * Generate secu-item string for the current payment.
   *
   * Each item will be a concatenation of:
   * <code>
   * product_id|sku|product_name|quantity|total
   * </code>
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return string
   *   The secu-item string.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  protected function generateSecuItems(OrderInterface $order) {
    $secu_items = '';
    foreach ($order->getItems() as $order_item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariation $ordered_entity */
      $ordered_entity = $order_item->getPurchasedEntity();
      if ($ordered_entity === NULL) {
        throw new PaymentGatewayException('Order contained an order item with an invalid product item.');
      }
      /*
       * Format:
       * [itemcode|itemskew|itemdesc|itempric|itemquan|itemtota]
       * [entity_id|sku|title|price|quantity|total]
       */

      $data = [
        $order_item->getPurchasedEntityId(),
        $ordered_entity->getSku(),
        // Sanitize the item name as SH servers might escape quotes with a
        // backslash. Causing secuString validations to fail.
        preg_replace('/[\'"]/', '', $ordered_entity->getOrderItemTitle()),
        $this->formatPrice($order_item->getUnitPrice()),
        $order_item->getQuantity(),
        $this->formatPrice($order_item->getTotalPrice()),
      ];
      $secu_items .= '[' . implode('|', $data) . ']';
    }

    return $secu_items;
  }

  /**
   * Fetch all potential order data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   The order object.
   * @param \Drupal\profile\Entity\ProfileInterface|null $billing_profile
   *   The billing profile.
   * @param \Drupal\commerce_price\Price|null $amount
   *   The transaction amount if any.
   *
   * @return array
   *   The order data.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getSecureHostingOrderData(?OrderInterface $order, ProfileInterface $billing_profile = NULL, Price $amount = NULL) {
    $transaction_amount = $amount ?: $order->getTotalPrice();
    $currency_code = $transaction_amount->getCurrencyCode();

    $data = [
      'shreference' => $this->configuration['shreference'],
      'checkcode' => $this->configuration['checkcode'],
      'filename' => isset($this->configuration['filename']) ? $this->configuration['shreference'] . '/' . $this->configuration['filename'] : NULL,
      'secuitems' => $order ? $this->generateSecuItems($order) : NULL,
      'orderid' => $order ? $order->id() : NULL,

      'transactionamount' => $this->formatPrice($transaction_amount),
      'transactioncurrency' => $currency_code,
      'transactionsymbol' => $this->getCurrencySymbol($currency_code),
      'subtotal' => $order ? $this->formatPrice($order->getSubtotalPrice()) : NULL,
      /*'transactiontax' => sprintf('%01.2f', $order->tax()),
      'shippingcharge' => sprintf('%01.2f', $order->shipping()),*/

      'cardholdersemail' => $order ? $order->getEmail() : NULL,

      // Extra information.
      'storename' => \Drupal::config('system.site')->get('name'),
      'ordernumber' => $order ? $order->getOrderNumber() : NULL,
    ];

    // Since requires_billing_information can be FALSE, the order is
    // not guaranteed to have a billing profile.
    $billing_profile = $billing_profile ?: $order->getBillingProfile();
    if ($billing_profile && $billing_profile->hasField('address')) {
      // NB: Is there a way to get the shipping info as well?
      /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
      $address = $billing_profile->get('address')->first();
      if ($address) {
        $data['cardholdersname'] = $address->getGivenName() . ' ' . $address->getFamilyName();
        $data['company'] = $address->getOrganization();
        $data['cardholderaddr1'] = $address->getAddressLine1();
        $data['cardholderaddr2'] = $address->getAddressLine2();
        $data['cardholdercity'] = $address->getLocality();
        $data['cardholderstate'] = $address->getAdministrativeArea();
        $data['cardholderpostcode'] = $address->getPostalCode();
        $data['cardholdercountry'] = $address->getCountryCode();
        /*$data['cardholdertelephonenumber'] = $data['shippingtelephonenumber'] = $address->getTelephone();*/
      }
    }

    $context['order'] = &$order;
    $context['billing_profile'] = &$billing_profile;
    $context['amount'] = &$amount;

    $this->moduleHandler->alter('commerce_securehosting_order_data', $data, $context);

    return $data;
  }

  /**
   * Format a price into a compatible Secure Hosting format.
   *
   * @param \Drupal\commerce_price\Price $price
   *   Price to format.
   *
   * @return string
   *   The formatted price.
   */
  protected function formatPrice(Price $price) {
    return sprintf('%01.2f', $price->getNumber());
  }

  /**
   * Get error message for a given code.
   *
   * @param string $error_code
   *   The error code.
   *
   * @return string
   *   The error message.
   */
  protected function getErrorMessage(string $error_code) {
    $error_codes = [
      'DATABASE_ERROR' => 'Please contact support (ref DBE).',
      'INVALID_LOGIN' => 'Invalid login details (ref ILD).',
      'TRANSACTION_ID' => 'Transaction ID is missing.',
      'UPG_PASS' => 'UPG Password is missing.',
      'NO_XML_PASSED' => 'Please contact support (ref NXP).',
      'BAD_XML_PASSED' => 'Please contact support (ref BXP).',
      'DATA_ERROR' => 'Please contact support (ref DAE).',
      'CREDIT_ERROR' => 'Please contact support (ref CAE).',
      'UPG_ERROR' => 'Please contact support (ref UPE).',
      'TRANSACTION_ERROR' => 'Please contact support (ref TRE).',
    ];

    return $error_codes[$error_code] ?: 'Unknown error code. Secure Hosting might be offline.';
  }

  /**
   * Assert that certain field values are present.
   *
   * @param array $fields
   *   The fields to check for.
   * @param array $data
   *   The data to search through.
   * @param string $message
   *   The error message prefix.
   *
   * @return array
   *   The field values if they're all set.
   *   Throws an InvalidRequestException exception otherwise.
   *
   * @throws \Drupal\commerce_payment\Exception\InvalidRequestException
   */
  protected function assertNonEmptyFields(array $fields, array $data, $message = 'Required field(s) are missing') {
    $active_fields = [];
    $missing = [];

    foreach ($fields as $field) {
      if (isset($data[$field])) {
        $active_fields[$field] = $data[$field];
      }
      else {
        $missing[$field] = $field;
      }
    }

    if ($missing) {
      throw new InvalidRequestException(sprintf('%s ("%s").', $message, implode(' ', $missing)));
    }

    return $active_fields;
  }

  /**
   * Get the render array for test transactions.
   *
   * @see https://www.securehosting.com/download/SHP-Technical-Integration-Guide.pdf
   * Check Appendix C
   * @see https://www.securehosting.com/download/Monek_test_Script3.0.3.pdf
   * @see https://documentation.monek.com/#appendix-c-test-card-numbers
   */
  public function helpRenderArray() {
    return [
      '#type' => 'details',
      '#title' => $this->t('Secure Hosting Testing Documentation'),
      '#open' => FALSE,
      '#attributes' => [
        'class' => ['securehosting-help'],
      ],
      'test_cards' => [
        '#type' => 'link',
        '#title' => $this->t('Test card numbers.'),
        '#url' => Url::fromUri('https://documentation.monek.com/#appendix-c-test-card-numbers'),
        '#attributes' => [
          'target' => '_blank',
          'rel' => 'noopener noreferrer',
        ],
      ],
    ];
  }

  /**
   * Get symbol from currency code.
   *
   * @param string $currency_code
   *   The currency code to look up.
   *
   * @return string
   *   The currency symbol.
   */
  protected function getCurrencySymbol(string $currency_code) {
    return $this->currencyRepository->get($currency_code)->getSymbol();
  }

}
