<?php

namespace Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway;

use CommerceGuys\Intl\Currency\CurrencyRepositoryInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\profile\Entity\ProfileInterface;
use GuzzleHttp\ClientInterface;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the On-site payment gateway for Secure Hosting.
 *
 * @CommercePaymentGateway(
 *   id = "securehosting_onsite",
 *   label = @Translation("Secure Hosting (On-site) (Experimental)"),
 *   display_label = @Translation("Secure Hosting On-site (Experimental)"),
 *   modes = {
 *     "test" = @Translation("Test"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "add-payment" = "Drupal\commerce_securehosting\PluginForm\Onsite\PaymentAddForm",
 *     "refund-payment" = "Drupal\commerce_securehosting\PluginForm\Onsite\PaymentRefundForm",
 *     "capture-payment" = "Drupal\commerce_securehosting\PluginForm\Onsite\PaymentCaptureForm",
 *     "void-payment" = "Drupal\commerce_payment\PluginForm\PaymentVoidForm",
 *     "add-payment-method" = "Drupal\commerce_securehosting\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_payment\PluginForm\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = TRUE
 * )
 */
class SecureHostingOnsiteGateway extends OnsitePaymentGatewayBase implements
    SecureHostingOnsiteGatewayInterface,
    SupportsRefundsInterface,
    SupportsAuthorizationsInterface {

  use SecureHostingGatewayTrait;

  // Type of requests.
  public const TYPE_REFUND = 'refund';
  public const TYPE_TRANSACTION_PRE_AUTHORISE = 'pre_auth';
  public const TYPE_TRANSACTION_AUTHORISE = 'authorise';
  public const TYPE_AUTHORISATION = 'authorisation';
  public const TYPE_ADDITIONAL = 'additional';

  /**
   * The securehosting test URL.
   *
   * @var string
   */
  public const SECUREHOSTING_TEST_URL = 'https://test.secure-server-hosting.com/secutran/api.php';

  /**
   * The securehosting production URL.
   *
   * @var string
   */
  public const SECUREHOSTING_LIVE_URL = 'https://www.secure-server-hosting.com/secutran/api.php';

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \CommerceGuys\Intl\Currency\CurrencyRepositoryInterface $currency_repository
   *   The currency repository.
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LoggerChannelFactoryInterface $logger_channel_factory,
    CurrencyRepositoryInterface $currency_repository,
    ClientInterface $client,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->logger = $logger_channel_factory->get('commerce_securehosting');
    $this->currencyRepository = $currency_repository;
    $this->httpClient = $client;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('commerce_price.currency_repository'),
      $container->get('http_client'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shreference' => '',
      'checkcode' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['status'] = [
      '#theme' => 'status_messages',
      '#message_list' => [
        'warning' => [
          $this->t('This integration is currently experimental, so use at your own risk.'),
        ],
      ],
      '#status_headings' => [
        'warning' => $this->t('Warning message'),
      ],
      '#weight' => -1,
    ];

    $form['shreference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SH Reference'),
      '#attributes' => [
        'placeholder' => $this->t('shXXXXXX'),
      ],
      '#description' => $this->t('The Secure Hosting account reference.'),
      '#default_value' => $this->configuration['shreference'],
      '#required' => TRUE,
    ];

    $form['checkcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Check code'),
      '#description' => $this->t('The Secure Hosting second level security check code.'),
      '#default_value' => $this->configuration['checkcode'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['shreference'] = $values['shreference'];
      $this->configuration['checkcode'] = $values['checkcode'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isTest() {
    return $this->getMode() === 'test';
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayUrl() {
    if ($this->isTest()) {
      return static::SECUREHOSTING_TEST_URL;
    }

    return static::SECUREHOSTING_LIVE_URL;
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $this->assertNonEmptyFields(
      ['type', 'number', 'expiration', 'security_code'],
      $payment_details
    );

    $card_cv2 = $payment_details['security_code'];
    $card_number = $payment_details['number'];
    $card_expiry_month = $payment_details['expiration']['month'];
    $card_expiry_year = $payment_details['expiration']['year'];

    $order = $this->getCurrentOrder();

    // Send pre-auth request.
    $xml = $this->getTransactionXml(
      $order,
      $payment_method->getBillingProfile(),
      $order->getTotalPrice(),
      static::TYPE_TRANSACTION_PRE_AUTHORISE,
      [
        'card_number' => $card_number,
        'card_cv2' => $card_cv2,
        'card_expiry_month' => $card_expiry_month,
        'card_expiry_year' => substr($card_expiry_year, -2),
      ]
    );

    $xml_response = $this->sendXmlData($xml);
    if ((string) $xml_response->status !== 'OK') {
      $internal_error = $this->getErrorMessage($xml_response->statustext);
      throw new HardDeclineException("Could not verify the card: $internal_error. Reason: {$xml_response->reason}");
    }

    $payment_method->set('card_type', $payment_details['type']);
    // Only the last 4 numbers are safe to store.
    $payment_method->set('card_number', substr($card_number, -4));
    $payment_method->set('card_exp_month', $card_expiry_month);
    $payment_method->set('card_exp_year', $card_expiry_year);
    $expires = CreditCard::calculateExpirationTimestamp(
      $card_expiry_month,
      $card_expiry_year
    );

    // Store the remote ID returned by the request.
    $payment_method
      ->setRemoteId((string) $xml_response->reference)
      ->setExpiresTime($expires)
      ->save();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE, $extras = []) {
    $this->validateNewPayment($payment);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $order = $payment->getOrder();
    $amount = $payment->getAmount();

    $xml = $this->getTransactionXml(
      $order,
      $order->getBillingProfile(),
      $amount,
      static::TYPE_ADDITIONAL,
      [
        'reference' => $payment_method->getRemoteId(),
        'amount' => $this->formatPrice($amount),
        'currencycode' => $amount->getCurrencyCode(),
        'card_number' => $payment_method->get('card_number')->value,
        'card_expiry_month' => $payment_method->get('card_exp_month')->value,
        'card_expiry_year' => $payment_method->get('card_exp_year')->value,
        'card_cv2' => $extras['card_cv2'] ?? '',
      ]
    );

    $xml_response = $this->sendXmlData($xml);
    if ((string) $xml_response->status !== 'OK') {
      $internal_error = $this->getErrorMessage($xml_response->statustext);
      throw new HardDeclineException("Could not charge the card: $internal_error. Reason: {$xml_response->reason}");
    }

    // The unique ID of the transaction.
    $payment->setRemoteId((string) $xml_response->reference);
    $payment->setState('authorization');
    $payment->save();

    // The account password.
    $account_password = $extras['sh_password'] ?? NULL;

    $can_capture = $capture && $account_password;
    if ($can_capture) {
      // Password is provided, we can attempt to capture/authorise now.
      $xml = $this->getSimpleXml(static::TYPE_AUTHORISATION, $payment_method->getRemoteId(), $amount, $account_password);

      $xml_response = $this->sendXmlData($xml);
      if ((string) $xml_response->status !== 'OK') {
        $internal_error = $this->getErrorMessage($xml_response->statustext);
        throw new HardDeclineException("Could not capture the card: $internal_error. Reason: {$xml_response->reason}");
      }

      $payment->setState('completed');
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL, $extras = []) {
    $this->validateAuthorization($payment);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    if (empty($extras['sh_password'])) {
      throw new InvalidRequestException('Account password not specified.');
    }

    // The account password.
    $account_password = $extras['sh_password'];

    $xml = $this->getSimpleXml(static::TYPE_AUTHORISATION, $payment->getRemoteId(), $amount, $account_password);
    $xml_response = $this->sendXmlData($xml);

    if ((string) $xml_response->status !== 'OK') {
      $internal_error = $this->getErrorMessage((string) $xml_response->statustext);
      $message = "Could not capture payment: $internal_error";
      $this->logger->error($message);
      throw new HardDeclineException($message);
    }

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL, $extras = []) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    if ($payment->getCompletedTime() < strtotime('-180 days')) {
      throw new InvalidRequestException('Unable to refund a payment captured more than 180 days ago.');
    }

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);

    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    if (empty($extras['sh_password'])) {
      throw new InvalidRequestException('Account password not specified.');
    }

    // The account password.
    $account_password = $extras['sh_password'];

    $xml = $this->getSimpleXml(static::TYPE_REFUND, $payment->getRemoteId(), $amount, $account_password);

    $xml_response = $this->sendXmlData($xml);
    if ((string) $xml_response->status !== 'OK') {
      throw new PaymentGatewayException(
        'Secure hosting refund failed: ' . $this->getErrorMessage((string) $xml_response->statustext)
      );
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    /*$this->assertPaymentState($payment, ['authorization']);
    $payment->setState('authorization_voided');
    $payment->save();*/

    throw new InvalidRequestException('Void functionality is not currently supported.');
  }

  /**
   * Fetch the current order object.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order object.
   */
  public function getCurrentOrder() {
    /* @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $route_match = \Drupal::routeMatch();
    if (($order = $route_match->getParameter('commerce_order')) && $order instanceof OrderInterface) {
      // On the checkout page, fetch it from the current context.
      return $order;
    }

    /* Unlikely to ever reach this code block. */

    /* @var \Drupal\commerce_cart\CartProviderInterface $cart_provider */
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    $carts = $cart_provider->getCarts();
    $carts = array_filter($carts, function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      return $cart->hasItems();
    });

    if (count($carts)) {
      return $carts[0];
    }

    throw new PaymentGatewayException('Could not determine the order.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $operations = parent::buildPaymentOperations($payment);

    if (isset($operations['void']) && $operations['void']['access']) {
      /*$operations['void']['title'] = $this->t('Void (Unsupported)');
      $operations['void']['disabled'] = TRUE;*/
      $operations['void']['access'] = FALSE;
    }

    return $operations;
  }

  /**
   * Send XML data to Secure Hosting.
   *
   * @param string $xml
   *   The XML to send across.
   *
   * @return \SimpleXMLElement
   *   The XML element to process.
   */
  protected function sendXmlData(string $xml) {
    $response = $this->httpClient->post(
      $this->getGatewayUrl(),
      [
        'form_params' => [
          'xmldoc' => $xml,
        ],
      ]
    );

    return simplexml_load_string($response->getBody());
  }

  /**
   * Return transaction XML payload.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   The order object.
   * @param \Drupal\profile\Entity\ProfileInterface|null $billing_profile
   *   The billing profile.
   * @param \Drupal\commerce_price\Price|null $amount
   *   The amount to bill.
   * @param string $transaction_type
   *   The transaction type.
   * @param array $details
   *   Extra details to pass in.
   *
   * @return string
   *   The XML string.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getTransactionXml(
    ?OrderInterface $order,
    ?ProfileInterface $billing_profile,
    ?Price $amount,
    string $transaction_type,
    array $details = []): string {

    $type = in_array(
      $transaction_type,
      [static::TYPE_TRANSACTION_PRE_AUTHORISE, static::TYPE_TRANSACTION_AUTHORISE],
      TRUE
    )
      ? 'transaction' : $transaction_type;

    $xml = "<?xml version=\"1.0\"?>\n";
    $xml .= "<request>\n";
    $xml .= '  <type>' . $type . "</type>\n";
    if ($type === 'transaction') {
      $xml .= '  <authtype>' . $transaction_type . "</authtype>\n";
    }
    $xml .= "  <authentication>\n";
    $xml .= '    <shreference>' . $this->configuration['shreference'] . "</shreference>\n";
    $xml .= '    <checkcode>' . $this->configuration['checkcode'] . "</checkcode>\n";
    $xml .= "  </authentication>\n";
    $xml .= "  <transaction>\n";
    if ($transaction_type === static::TYPE_ADDITIONAL) {
      $this->assertNonEmptyFields(['reference'], $details);

      $data = $this->getSecureHostingOrderData($order, $billing_profile, $amount);

      $this->assertNonEmptyFields(
        ['transactioncurrency', 'transactionamount', 'secuitems', 'orderid'],
        $data
      );

      $xml .= '    <reference>' . $details['reference'] . "</reference>\n";
      $xml .= '    <currency>' . $data['transactioncurrency'] . "</currency>\n";
      $xml .= '    <transactionamount>' . $data['transactionamount'] . "</transactionamount>\n";
      $xml .= '    <secuitems><![CDATA[' . $data['secuitems'] . "]]></secuitems>\n";
      $xml .= '    <orderid>' . $data['orderid'] . "</orderid>\n";
      if (!empty($details['cv2'])) {
        // Add the CV2 if it's available.
        $xml .= '    <cv2>' . $details['cv2'] . "</cv2>\n";
      }
    }
    elseif ($type === 'transaction') {
      // Authorisation or pre-authorisation.
      $data = $this->getSecureHostingOrderData($order, $billing_profile, $amount);

      // Assert fields supplied by the user are non-empty.
      $this->assertNonEmptyFields(
        [
          'cardholdersname',
          /*'cardholdersemail',
          'cardholderstate',*/
          'cardholderaddr1',
          'cardholderaddr2',
          'cardholdercity',
          'cardholderpostcode',
          'cardholdercountry',
        ],
        $data
      );

      $this->assertNonEmptyFields(
        ['card_number', 'card_cv2', 'card_expiry_month', 'card_expiry_year'],
        $details
      );

      $card_number = $details['card_number'];
      $card_cv2 = $details['card_cv2'];
      $card_expiry_month = $details['card_expiry_month'];
      $card_expiry_year = $details['card_expiry_year'];

      $xml .= '    <cardnumber>' . $card_number . "</cardnumber>\n";
      $xml .= '    <cv2>' . $card_cv2 . "</cv2>\n";
      $xml .= '    <cardexpiremonth>' . $card_expiry_month . "</cardexpiremonth>\n";
      $xml .= '    <cardexpireyear>' . $card_expiry_year . "</cardexpireyear>\n";
      // Cardholder details.
      $xml .= '    <cardholdersname>' . $data['cardholdersname'] . "</cardholdersname>\n";
      $xml .= '    <cardholdersemail>' . $data['cardholdersemail'] . "</cardholdersemail>\n";
      $xml .= '    <cardholderaddr1>' . $data['cardholderaddr1'] . "</cardholderaddr1>\n";
      $xml .= '    <cardholderaddr2>' . $data['cardholderaddr2'] . "</cardholderaddr2>\n";
      $xml .= '    <cardholdercity>' . $data['cardholdercity'] . "</cardholdercity>\n";
      $xml .= '    <cardholderstate>' . $data['cardholderstate'] . "</cardholderstate>\n";
      $xml .= '    <cardholderpostcode>' . $data['cardholderpostcode'] . "</cardholderpostcode>\n";
      $xml .= '    <cardholdercountry>' . $data['cardholdercountry'] . "</cardholdercountry>\n";
      // Order details.
      $xml .= '    <orderid>' . $data['orderid'] . "</orderid>\n";
      $xml .= '    <subtotal>' . $data['subtotal'] . "</subtotal>\n";
      $xml .= '    <transactionamount>' . $data['transactionamount'] . "</transactionamount>\n";
      $xml .= '    <transactioncurrency>' . $data['transactioncurrency'] . "</transactioncurrency>\n";
      $xml .= '    <secuitems><![CDATA[' . $data['secuitems'] . "]]></secuitems>\n";
    }
    else {
      throw new InvalidRequestException('Invalid transaction type specified');
    }

    $xml .= "  </transaction>\n";
    $xml .= '</request>';
    return $xml;
  }

  /**
   * Return a simple request XML.
   *
   * @param string $request_type
   *   The request type, either refund orn authorisation.
   * @param string $remote_id
   *   The remote transaction ID.
   * @param \Drupal\commerce_price\Price $amount
   *   The price to apply.
   * @param string $account_pass
   *   The account password.
   *
   * @return string
   *   The XML file.
   */
  protected function getSimpleXml(string $request_type, string $remote_id, Price $amount, string $account_pass): string {
    $xml = '<?xml version ="1.0"?>';
    $xml .= "<request>\n";
    $xml .= '  <type>' . $request_type . "</type>\n";
    $xml .= "  <authentication>\n";
    $xml .= '    <shreference>' . $this->configuration['shreference'] . "</shreference>\n";
    $xml .= '    <password>' . $account_pass . "</password>\n";
    $xml .= "  </authentication>\n";
    $xml .= "  <transaction>\n";
    $xml .= '    <reference>' . $remote_id . "</reference>\n";
    $xml .= '    <amount>' . $this->formatPrice($amount) . "</amount>\n";
    $xml .= "  </transaction>\n";
    $xml .= '</request>';
    return $xml;
  }

  /**
   * Validate a new payment.
   */
  protected function validateNewPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    if ($payment_method === NULL) {
      throw new InvalidArgumentException('The provided payment has no payment method referenced.');
    }

    if ($payment_method->isExpired()) {
      throw new HardDeclineException('The provided payment method has expired.');
    }
  }

  /**
   * Validate a payments authorization.
   */
  protected function validateAuthorization(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment_method = $payment->getPaymentMethod();
    if ($payment_method === NULL) {
      throw new InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    if ($payment->isExpired()) {
      throw new \InvalidArgumentException('Authorizations are guaranteed for up to 29 days.');
    }
    if (empty($payment->getRemoteId())) {
      throw new \InvalidArgumentException('Could not retrieve the transaction ID.');
    }
  }

}
