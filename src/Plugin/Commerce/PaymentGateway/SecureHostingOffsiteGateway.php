<?php

namespace Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway;

use CommerceGuys\Intl\Currency\CurrencyRepositoryInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Off-site Redirect payment gateway for Secure Hosting.
 *
 * @CommercePaymentGateway(
 *   id = "securehosting_offsite_redirect",
 *   label = @Translation("Secure Hosting (Off-site redirect)"),
 *   display_label = @Translation("Secure Hosting Off-site"),
 *   modes = {
 *     "test" = @Translation("Test"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_securehosting\PluginForm\OffsiteRedirect\SecureHostingRedirectForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE
 * )
 */
class SecureHostingOffsiteGateway extends OffsitePaymentGatewayBase implements SecureHostingOffsiteGatewayInterface {

  use SecureHostingGatewayTrait;

  /**
   * The securehosting test URL.
   *
   * @var string
   */
  public const SECUREHOSTING_TEST_URL = 'https://test.secure-server-hosting.com/secutran/secuitems.php';

  /**
   * The securehosting production URL.
   *
   * @var string
   */
  public const SECUREHOSTING_LIVE_URL = 'https://www.secure-server-hosting.com/secutran/secuitems.php';

  /**
   * The securehosting secu-string generation URL.
   *
   * @var string
   */
  public const SECUREHOSTING_SECUSTRING_URL = 'https://www.secure-server-hosting.com/secutran/create_secustring.php';

  /**
   * The securehosting callback processing URL.
   *
   * @var string
   */
  public const SECUREHOSTING_CALLBACK_URL = 'https://www.secure-server-hosting.com/secutran/ProcessCallbacks.php';

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \CommerceGuys\Intl\Currency\CurrencyRepositoryInterface $currency_repository
   *   The currency repository.
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LoggerChannelFactoryInterface $logger_channel_factory,
    CurrencyRepositoryInterface $currency_repository,
    ClientInterface $client,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $logger_channel_factory->get('commerce_securehosting');
    $this->currencyRepository = $currency_repository;
    $this->httpClient = $client;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('commerce_price.currency_repository'),
      $container->get('http_client'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shreference' => '',
      'checkcode' => '',
      'filename' => '',
      'auto_submit' => TRUE,
      'auto_submit_timeout' => 0,
      'advanced_secuitems' => [
        'enabled' => TRUE,
        'shared_secret' => '',
        'phrase' => '',
        'referrer' => Url::fromUri('internal:/commerce_securehosting/referrer', ['absolute' => TRUE])->toString(),
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shreference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SH Reference'),
      '#attributes' => [
        'placeholder' => $this->t('shXXXXXX'),
      ],
      '#description' => $this->t('The Secure Hosting account reference.'),
      '#default_value' => $this->configuration['shreference'],
      '#required' => TRUE,
    ];

    $form['checkcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Check code'),
      '#description' => $this->t('The Secure Hosting second level security check code.'),
      '#default_value' => $this->configuration['checkcode'],
      '#required' => TRUE,
    ];

    $form['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secure Hosting template filename'),
      '#pattern' => '.+\.html?',
      '#attributes' => [
        'placeholder' => $this->t('template.html'),
      ],
      '#description' => $this->t('The Secure Hosting filename. The file name must end in %format1 or %format2.', [
        '%format1' => '.htm',
        '%format2' => '.html',
      ]),
      '#default_value' => $this->configuration['filename'],
      '#required' => TRUE,
    ];

    /** @var \Drupal\commerce_payment\Form\PaymentGatewayForm $form_object */
    $form_object = $form_state->getFormObject();

    if (($gateway_entity = $form_object->getEntity()) && $gateway_entity->id()) {
      // Replicates getNotifyUrl functionality.
      $form['callback_url'] = [
        '#type' => 'item',
        '#title' => $this->t('Callback url'),
        '#markup' => Url::fromRoute('commerce_payment.notify', [
          'commerce_payment_gateway' => $gateway_entity->id(),
        ], ['absolute' => TRUE])->toString(),
        '#description' => $this->t(
          'The callback url of the cart. Used for handling transaction notifications.'
        ),
      ];
    }

    $form['auto_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto submit'),
      '#description' => $this->t('Auto submit the form. Can be disabled while testing. Should be enabled on production servers.'),
      '#default_value' => $this->configuration['auto_submit'],
    ];

    $form['auto_submit_timeout'] = [
      '#type' => 'number',
      '#min' => 0,
      '#size' => 7,
      '#field_suffix' => $this->t('millisecond(s)'),
      '#title' => $this->t('Auto submit timeout'),
      '#description' => $this->t('How long to wait before submitting the form.'),
      '#default_value' => $this->configuration['auto_submit_timeout'],
    ];

    $secu_items_enabled = $this->configuration['advanced_secuitems']['enabled'];
    $secu_items_states = [
      'enabled' => [
        ':input[name="configuration[securehosting_offsite_redirect][advanced_secuitems][enabled]"]' => ['checked' => TRUE],
      ],
      'required' => [
        ':input[name="configuration[securehosting_offsite_redirect][advanced_secuitems][enabled]"]' => ['checked' => TRUE],
      ],
    ];

    $form['advanced_secuitems'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced SecuItems Security Options'),
      '#description' => $this->t('Information put in here should match what is in your <a href="@uri" target="_blank" rel="noreferrer noopener">Secure Hosting admin</a>.', [
        '@uri' => 'https://www.secure-server-hosting.com/secutran/client.php?option=4',
      ]),
      '#open' => TRUE,
      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable SecuItems'),
        '#default_value' => $secu_items_enabled,
      ],
      'phrase' => [
        '#type' => 'textfield',
        '#title' => $this->t('Phrase'),
        '#description' => $this->t('A phrase to be used to further encrypt your data sent through secuitems. (between 6 and 9 characters):'),
        '#minlength' => 6,
        '#maxlength' => 9,
        '#default_value' => $this->configuration['advanced_secuitems']['phrase'],
        '#states' => $secu_items_states,
      ],
      'shared_secret' => [
        '#type' => 'textfield',
        '#title' => $this->t('Shared secret'),
        '#description' => $this->t('A shared secret that will be used to verify the notification callbacks. We recommend setting this value.'),
        '#minlength' => 6,
        '#maxlength' => 9,
        '#default_value' => $this->configuration['advanced_secuitems']['shared_secret'],
        '#states' => $secu_items_states,
      ],
      'referrer' => [
        '#type' => 'url',
        '#title' => $this->t('Shopping cart referrer'),
        '#default_value' => $this->getRefererUrl(),
        '#description' => $this->t(
          'This should be entered on the Advanced Settings screen (Settings >> Advanced Settings >> The full URL referrer of your shopping cart)'
        ),
        '#states' => $secu_items_states,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    if (!empty($values['advanced_secuitems']['enabled'])) {
      $form['advanced_secuitems']['phrase']['#required'] = TRUE;
      $form['advanced_secuitems']['shared_secret']['#required'] = TRUE;
      $form['advanced_secuitems']['referrer']['#required'] = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['shreference'] = $values['shreference'];
      $this->configuration['checkcode'] = $values['checkcode'];
      $this->configuration['filename'] = $values['filename'];
      $this->configuration['auto_submit'] = $values['auto_submit'];
      $this->configuration['auto_submit_timeout'] = $values['auto_submit_timeout'];
      $this->configuration['advanced_secuitems']['enabled'] = $values['advanced_secuitems']['enabled'];
      $this->configuration['advanced_secuitems']['shared_secret'] = $values['advanced_secuitems']['shared_secret'];
      $this->configuration['advanced_secuitems']['phrase'] = $values['advanced_secuitems']['phrase'];
      $this->configuration['advanced_secuitems']['referrer'] = $values['advanced_secuitems']['referrer'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function autoSubmits() {
    return $this->configuration['auto_submit'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoSubmitTimeout() {
    return $this->configuration['auto_submit_timeout'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRefererUrl() {
    return $this->configuration['advanced_secuitems']['referrer'];
  }

  /**
   * {@inheritdoc}
   */
  public function isTest() {
    return $this->getMode() === 'test';
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayUrl() {
    if ($this->isTest()) {
      return static::SECUREHOSTING_TEST_URL;
    }

    return static::SECUREHOSTING_LIVE_URL;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // On return from the off-site page.
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    if ($request->headers->get('Referer') !== static::SECUREHOSTING_CALLBACK_URL) {
      return new Response('INCORRECT_REFERER', Response::HTTP_BAD_REQUEST);
    }

    $required_fields = ['orderid', 'transactionnumber'];

    $shared_secret = $this->configuration['advanced_secuitems']['shared_secret'];

    if ($shared_secret) {
      $required_fields[] = 'verify';
    }

    $missing_fields = [];
    foreach ($required_fields as $field) {
      if (empty($request->query->get($field))) {
        $missing_fields[$field] = TRUE;
      }
    }

    if ($missing_fields) {
      $this->logger->error(sprintf('Missing onNotify fields (%s)', implode(',', $missing_fields)));
      return new Response('MISSING_FIELDS');
    }

    $order_id = $request->query->get('orderid');
    $transaction_number = (string) $request->query->get('transactionnumber');
    /* $transaction_time = $request->query->get('transactiontime'); */

    if ($shared_secret) {
      $verify = $request->query->get('verify');
      if ($verify !== hash('sha1', $shared_secret . $transaction_number . $shared_secret)) {
        $this->logger->warning($this->t('Incorrect verification hash received for order %order', [
          '%order' => $order_id,
        ]));
        return new Response('BAD_VERIFY');
      }
    }

    $order = Order::load($order_id);
    if ($order === NULL) {
      $this->logger->warning($this->t('Order %order does not exist.', [
        '%order' => $order_id,
      ]));
      return new Response('INVALID_ORDER_ID');
    }

    if ($transaction_number === '-1') {
      // Failed transaction.
      $state = 'failed';
      $remote_id = NULL;
      $remote_state = $request->query->get('failurereason');
    }
    else {
      $cv2_avs_result = $request->query->get('cv2avsresult');
      $upg_card_type = $request->query->get('upgcardtype');
      $upg_auth_code = $request->query->get('upgauthcode');

      $state = 'completed';
      $remote_id = $transaction_number;
      $remote_state = $cv2_avs_result . '|' . $upg_card_type . '|' . $upg_auth_code;
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create(
      [
        'state' => $state,
        'amount' => $order->getBalance(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $remote_id,
        'remote_state' => $remote_state,
      ]
    );
    $payment->save();

    // Successfully handled the callback.
    return new Response('success');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function buildFormData(PaymentInterface $payment, array $form, FormStateInterface $form_state) {
    $data = $this->getSecureHostingOrderData($payment->getOrder());

    // Callback for verifying the transaction.
    $data['callbackurl'] = $this->getNotifyUrl()->toString();
    $data['callbackdata'] = 'orderid|#orderid|transactionamount|#transactionamount|verify|#verify';

    if (!empty($this->configuration['advanced_secuitems']['enabled'])) {
      // Generate the advanced secu-string to avoid tampering.
      $data['secuString'] = $this->fetchSecuString($data);
    }

    return $data;
  }

  /**
   * Generate the secu-items string.
   *
   * @param array $data
   *   The form data.
   * @param array $fields
   *   The fields to hash.
   *
   * @return string
   *   The secu-items string.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  protected function fetchSecuString(array $data, array $fields = ['transactionamount', 'transactioncurrency']) {
    $fields[] = 'secuitems';

    $extra_fields = $this->assertNonEmptyFields($fields, $data);

    $response = $this->httpClient->post(
      static::SECUREHOSTING_SECUSTRING_URL,
      [
        'form_params' => [
          'shreference' => $this->configuration['shreference'],
          'secuphrase' => $this->configuration['advanced_secuitems']['phrase'],
        ] + $extra_fields,
        'headers' => [
          'Referer' => $this->getRefererUrl(),
        ],
      ]
    );

    $result = $response->getBody();

    if ($result === FALSE) {
      throw new InvalidResponseException('Could not generate secu-item string.');
    }

    $result = trim($result);

    if (preg_match('/value=\"([a-zA-Z0-9]{32})\"/', $result, $matches)) {
      return $matches[1];
    }

    throw new InvalidResponseException('Invalid secu-item string received.');
  }

}
