<?php

namespace Drupal\commerce_securehosting\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for handling the secure hosting redirects.
 */
class SecureHostingRedirectForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /* @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    /* @var \Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway\SecureHostingOffsiteGatewayInterface $payment_gateway_plugin */
    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $data = $payment_gateway_plugin->buildFormData($payment, $form, $form_state);
    $data['returnurl'] = $form['#return_url'];

    $form = $this->buildRedirectForm($form, $form_state, $payment_gateway_plugin->getGatewayUrl(), $data, self::REDIRECT_POST);

    if ($payment_gateway_plugin->isTest()) {
      $form['securehosting_help'] = $payment_gateway_plugin->helpRenderArray();
      $form['securehosting_help']['#weight'] = -1;
    }

    // Remove the default auto-submit handler.
    if (($key = array_search('commerce_payment/offsite_redirect', $form['#attached']['library'], TRUE)) !== FALSE) {
      unset($form['#attached']['library'][$key]);
    }

    if ($payment_gateway_plugin->autoSubmits()) {
      $form['#attached']['drupalSettings']['commerce_securehosting']['timeout'] = $payment_gateway_plugin->getAutoSubmitTimeout();
      $form['#attached']['library'][] = 'commerce_securehosting/offsite_redirect';
    }

    return $form;
  }

}
