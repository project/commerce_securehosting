<?php

namespace Drupal\commerce_securehosting\PluginForm\Onsite;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a class for adding payment information.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway\SecureHostingOnsiteGatewayInterface $plugin */

    $plugin = $this->plugin;

    if ($plugin->isTest()) {
      $form['securehosting_help'] = $plugin->helpRenderArray();
      $form['securehosting_help']['#weight'] = -1;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);

    $form_state->setValue(
      array_merge($element['#parents'], ['number']),
      str_replace([' ', '-'], '', $values['number'])
    );

    return parent::validateCreditCardForm($element, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element = parent::buildCreditCardForm($element, $form_state);

    return $element;
  }

}
