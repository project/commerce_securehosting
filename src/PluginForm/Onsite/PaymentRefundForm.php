<?php

namespace Drupal\commerce_securehosting\PluginForm\Onsite;

use Drupal\commerce_payment\PluginForm\PaymentRefundForm as BasePaymentRefundForm;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a class for refunding payments.
 */
class PaymentRefundForm extends BasePaymentRefundForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['sh_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Secure Hosting password'),
      '#description' => $this->t('The account password is required for issuing refunds.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway\SecureHostingOnsiteGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    $payment_gateway_plugin->refundPayment($payment, $amount, [
      'sh_password' => $values['sh_password'],
    ]);
  }

}
