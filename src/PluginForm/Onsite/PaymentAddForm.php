<?php

namespace Drupal\commerce_securehosting\PluginForm\Onsite;

use Drupal\commerce_payment\PluginForm\OnsitePaymentAddForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a class for refunding payments.
 */
class PaymentAddForm extends OnsitePaymentAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['sh_cvv'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CVV'),
      '#description' => $this->t('The CVV if available, will increase authorisation rates and can provide lower bank fees.'),
      '#attributes' => ['autocomplete' => 'off'],
      '#required' => FALSE,
      '#maxlength' => 4,
      '#size' => 6,
    ];

    $form['sh_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Secure Hosting password'),
      '#description' => $this->t('The account password is required for capturing/authorising transactions.'),
      '#states' => [
        'required' => [
          ':input[name="payment[transaction_type]"]' => ['value' => 'capture'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $capture = ($values['transaction_type'] === 'capture');
    /* @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment->amount = $values['amount'];
    /* @var \Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway\SecureHostingOnsiteGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    // Pass in additional options to the plugin.
    $payment_gateway_plugin->createPayment($payment, $capture, [
      'sh_password' => $values['sh_password'],
      'card_cv2' => $values['sh_cvv'],
    ]);
  }

}
