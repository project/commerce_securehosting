The files in this folder are example custom pages for uploading to Secure
Hosting. They should work without any modifications. Alternatively you can
use the official pages as a starting point by downloading 'Example Customisable
Files.zip' from Secure Hosting's documentation page[1].

[1] https://www.secure-server-hosting.com/secutran/documentation/index.php
