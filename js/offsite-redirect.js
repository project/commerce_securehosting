/**
 * @file
 * Defines behaviors for the secure hosting payment redirect form.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the secureHostingPaymentRedirect behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the secureHostingPaymentRedirect behavior.
   */
  Drupal.behaviors.secureHostingPaymentRedirect = {
    attach: function (context, settings) {
      var timeout = 0;
      if (settings['commerce_securehosting'] && settings['commerce_securehosting']['timeout']) {
        timeout = settings['commerce_securehosting']['timeout'];
      }
      var redirectFunction = function () {
        // Programmatically click the submit button so that event listeners e.g. GTM form listeners are also triggered.
        // https://stackoverflow.com/a/12053166
        var $form = $('.payment-redirect-form', context);
        $form.find('[type="submit"]').click();
      };
      if (timeout > 0) {
        setTimeout(redirectFunction, timeout);
      }
      else {
        redirectFunction();
      }
    }
  };

})(jQuery, Drupal);
