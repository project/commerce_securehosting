INTRODUCTION
============

Provides integration between Drupal Commerce and Secure Hosting.

It provides onsite and offsite payments integration.

However the onsite integration is still currently experimental.

In order to use the onsite integration, you will also need to consider the
Payment Card Industry Data Security Standard (PCI:DSS) when your system 
captures card details. For more information, please see 
https://www.pcisecuritystandards.org/.

Template examples are also available in the `sh_custom_pages` folder.

REQUIREMENTS
============
This module relies on the [Commerce](https://www.drupal.org/project/commerce) 
module.

INSTALLATION
============

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
=============
1. Install and enable the Commerce payment service:
   Admin >> Commerce >> Configuration >> Payment gateways. 
   
   Follow the instructions on the configuration form.

EXTERNAL DOCUMENTATION
======================
- https://documentation.monek.com/#introduction
- https://www.securehosting.com/download/SHP-Technical-Integration-Guide.pdf

MAINTAINERS
===========

Current maintainers:
 * [codebymikey](https://www.drupal.org/u/codebymikey)

Development sponsored by:
- [Zodiac Media](https://www.zodiacmedia.co.uk/) - Zodiac Media is a Drupal 
  specialist website design and development company based in London.
