<?php

/**
 * @file
 * Hooks for the commerce_securehosting module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter order data before it's sent over to the server.
 *
 * @param array $data
 *   Order data to alter.
 * @param array $context
 *   An associative array of additional options, with the following elements:
 *   - 'order': The order entity if available.
 *   - 'billing_profile': The Billing profile entity if available.
 *   - 'amount': The order amount if available.
 *
 * @see \Drupal\commerce_securehosting\Plugin\Commerce\PaymentGateway\SecureHostingGatewayTrait::getSecureHostingOrderData
 */
function hook_commerce_securehosting_order_data_alter(array &$data, array &$context) {
  if ($context['billing_profile']) {
    // Attempt to specify a phone number from the billing profile entity.
    /* @var $billing_profile \Drupal\profile\Entity\ProfileInterface */
    $billing_profile = $context['billing_profile'];
    if ($billing_profile->hasField('field_phonenum')) {
      $data['cardholdertelephonenumber'] = $billing_profile->get('field_phonenum')->value;
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
